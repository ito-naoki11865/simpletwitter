package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorComments = new ArrayList<String>();

		String text = request.getParameter("text");
		String messageId = request.getParameter("id");
		Integer id = Integer.parseInt(messageId);

		if (!isValid(text, errorComments)) {
			session.setAttribute("errorComments", errorComments);
			response.sendRedirect("./");
			return;
		}

		Comment comment = new Comment();
		comment.setText(text);

		User user = (User) session.getAttribute("loginUser");
		comment.setUserId(user.getId());

		new CommentService().insert(comment, id);
		response.sendRedirect("./");
	}
	private boolean isValid(String text, List<String> errorComments) {

		if (StringUtils.isBlank(text)) {
			errorComments.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorComments.add("140文字以下で入力してください");
		}

		if (errorComments.size() != 0) {
			return false;
		}
		return true;
	}
}